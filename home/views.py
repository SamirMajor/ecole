from django.shortcuts import render , redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate , login
from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse
from .models import (Amenities, Ecole, EcoleReservation)
from django.db.models import Q



def check_reservation(start_date  , end_date ,uid):
    qs = EcoleReservation.objects.filter(
        start_date__lte=start_date,
        end_date__gte=end_date,
        ecole__uid = uid
    )

    return True

def home(request):
    amenities_objs = Amenities.objects.all()
    ecoles_objs = Ecole.objects.all()

    sort_by = request.GET.get('sort_by')
    search = request.GET.get('search')
    amenities = request.GET.getlist('amenities')
    print(amenities)
    if sort_by:
        if sort_by == 'ASC':
            ecoles_objs = ecoles_objs.order_by('ecole_price')
        elif sort_by == 'DSC':
            ecoles_objs = ecoles_objs.order_by('-ecole_price')

    if search:
        ecoles_objs = ecoles_objs.filter(
            Q(ecole_name__icontains = search) |
            Q(description__icontains = search))


    if len(amenities):
        ecoles_objs = ecoles_objs.filter(amenities__amenity_name__in = amenities).distinct()



    context = {'amenities_objs' : amenities_objs , 'ecoles_objs' : ecoles_objs , 'sort_by' : sort_by
        , 'search' : search , 'amenities' : amenities}
    return render(request , 'home.html' ,context)



def ecole_detail(request,uid):
    ecole_obj = Ecole.objects.get(uid = uid)

    if request.method == 'POST':
        checkin = request.POST.get('checkin')
        checkout= request.POST.get('checkout')
        ecole = Ecole.objects.get(uid = uid)
        if not check_reservation(checkin ,checkout  , uid , ecole.room_count):
            messages.warning(request, 'ecole is already booked in these dates ')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        EcoleReservation.objects.create(ecole=ecole , user = request.user , start_date=checkin
                                    , end_date = checkout)

        messages.success(request, 'Your reservation has been saved')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))




    return render(request , 'ecole_detail.html' ,{
        'ecoles_obj' :ecole_obj
    })

def login_page(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user_obj = User.objects.filter(username = username)

        if not user_obj.exists():
            messages.warning(request, 'Account not found ')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        user_obj = authenticate(username = username , password = password)
        if not user_obj:
            messages.warning(request, 'Invalid password ')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        login(request , user_obj)
        return redirect('/')


        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    return render(request ,'login.html')


def register_page(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user_obj = User.objects.filter(username = username)

        if user_obj.exists():
            messages.warning(request, 'Username already exists')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        user = User.objects.create(username = username)
        user.set_password(password)
        user.save()
        return redirect('/')

    return render(request , 'register.html')
