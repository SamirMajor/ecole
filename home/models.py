from django.contrib.auth.models import User
from django.db import models
import uuid


class BaseModel(models.Model):
    uid = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now_add=True)

    class Meta:
        abstract = True


class Amenities(BaseModel):
    amenity_name = models.CharField(max_length=100)

    def __str__(self) -> str:
        return self.amenity_name


class Ecole(BaseModel):
    ecole_name = models.CharField(max_length=100)
    description = models.TextField()
    amenities = models.ManyToManyField(Amenities)

    def __str__(self) -> str:
        return self.ecole_name


class EcoleImages(BaseModel):
    ecole = models.ForeignKey(Ecole, related_name="images", on_delete=models.CASCADE)
    images = models.ImageField(upload_to="ecoles")


class EcoleReservation(BaseModel):
    ecole = models.ForeignKey(Ecole, related_name="ecole_reservation", on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name="user_reservation", on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
